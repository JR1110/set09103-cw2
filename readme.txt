This is my project for the second coursework.

The web app will be a fantasy football app based on the premier league but may
be extended to include other big leagues (ligue 1, La Liga, bundesliga, etc ..)

To run this application you need to do the following :

    Run levinux (or any other suitable linux distro) 
    and have the application downloaded from the git repository
      
        https://JR1110@bitbucket.org/JR1110/set09103-cw1.git

    Go into the src folder
  
    Enter the following into the command line - python init_db.py
    This will run the programme to initialize the database

    Enter the following into the command line - python fantasyFootball.py
    This will run the application

    Then in your browser of choice go to - http://localhost:5000
    You have now enetered the application and can go about using it from here

If you wish to stop running the application :
    
    Go into the command line where the application is running

    press ctrl + C

    This will shut down the applicationa and it will no longer be accessable
    from the URL


Written by Josh Renwick


TODO :

Transfers page,
Subsitution page
Page for inputted matches, scores and players