from flask import Flask, url_for, render_template, g, request, session, abort, redirect
from functools import wraps
import sqlite3
import bcrypt

app = Flask(__name__)
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
uTeam = []
moneyRemaining = 250.0
team = []
selectedTeam = []

db_location = '/var/fantasyFootballDatabase.db'

class player:
    def __init__(self, ID, name, born, position, squadNumber, countryFrom, value, club) :
        self.ID = ID
        self.name = name
        self.born = born
        self.position = position
        self.squadNumber = squadNumber
        self.countryFrom = countryFrom
        self.value = value
        self.club = club


for i in range (0,20):
    uTeam.append("")
for i in range(0,19):
    selectedTeam.append("")

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = sqlite3.connect(db_location)
        g.db = db
    return db

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()
        with app.open_resource('players.sql', mode='r') as p:
            db.cursor().executescript(p.read())
        db.commit()
        with app.open_resource('stadiums.sql', mode='r') as s:
            db.cursor().executescript(s.read())
        db.commit()
        with app.open_resource('users.sql', mode='r') as u:
            db.cursor().executescript(u.read())
        db.commit()

@app.teardown_appcontext
def close_db_connection(exception):
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

def check_auth(email, password):
    db = get_db()
    valid_email = ""
    valid_pwhash = ""
    sql = "SELECT * FROM users WHERE userName='" + email + "'"
    for row in db.cursor().execute(sql):
        valid_email = row[0]
        valid_pwhash = row[1]
    if(email == valid_email and valid_pwhash == bcrypt.hashpw(password.encode('utf-8'), valid_pwhash)):
        return True
    return False

def requires_login(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        status = session.get('logged_in', False)
        if not status:
            return redirect(url_for('.home'))
        return f(*args, **kwargs)
    return decorated

@app.route("/", methods=['GET', 'POST'])
def home() :
    sesh = ""
    if request.method == 'POST':
        user = request.form['userName']
        pW = request.form['password']
        if check_auth(user,pW):
            session['logged_in'] = True
            session['who'] = str(user)
            return redirect(url_for('.userHome'))
    try:
        if session['logged_in'] == True:
            sesh = "y"
    except KeyError:
        pass
    return render_template('home.html', sesh=sesh), 200

@app.route("/logout/")
def logout():
    session['logged_in'] = False
    return redirect(url_for('.home'))

@app.route("/addPlayer/")
def addPlayer():
    names = []
    clubs = []
    chosenPlayer = []
    chosenPlayer.append(player("","","","","","","",""))
    names.append("<option value='NP'> New Player </option>")
    db = get_db()
    sql = "SELECT playerID, playerName FROM player"
    for row in db.cursor().execute(sql):
        names.append("<option value='" + str(row[0]) + "'>" + row[1] + "</option>")
    clubSql = "SELECT clubID, clubName FROM club"
    for row in db.cursor().execute(clubSql):
        clubs.append("<option value='" + str(row[0]) + "'>" + row[1] + "</option>")
    selectedPlayer = request.args.get('p')
    if selectedPlayer == 'NP':
        return render_template('playerEntry.html', names=names,player=chosenPlayer, clubs=clubs)
    if selectedPlayer != '':
        playerSQL = "SELECT * FROM player WHERE playerID='" + str(selectedPlayer) + "'"
        for row in db.cursor().execute(playerSQL):
           chosenPlayer.insert(0,player(row[0], row[1], row[2], row[3], row[4],row[5], row[6], row[7]))
    return render_template('playerEntry.html', names=names,player=chosenPlayer[0], clubs=clubs), 200

@app.route("/addedPlayer/", methods=['POST'])
def addedPlayer():
    f = str(request.form.get('id'))
    db = get_db()
    if request.method == 'POST':
        if f == 'None':
            s = "INSERT INTO player(playerName, born, position, squadNumber,countryFrom, value, club) VALUES('" 
            s = s + str(request.form.get('name')) + "','" + str(request.form.get('born')) + "','" + str(request.form.get('position'))+ "','" + str(request.form.get('squadNumber')) + "','" + str(request.form.get('countryFrom')) + "'," + str(request.form.get('value')) + "," + str(request.form.get('club')) + ");"
            db.cursor().execute(s)
            db.commit()
        else:
            s = "UPDATE player SET playerName='" + str(request.form.get('name')) + "', born='" + str(request.form.get('born')) + "', position='" + str(request.form.get('position')) + "', squadNumber='" + str(request.form.get('squadNumber')) + "', countryFrom='" + str(request.form.get('countryFrom')) + "', value='" + str(request.form.get('value')) + "' WHERE playerID='" + f + "';"
            db.cursor().execute(s)
            db.commit()
    fileWrite = open('players.sql', 'a')
    fileWrite.write(s)
    fileWrite.close
    return redirect(url_for('addPlayer'))


def checkUser(username):
    db = get_db()
    i = 0
    sql = "SELECT userName FROM users WHERE userName='" + str(username) + "'"
    for row in db.cursor().execute(sql):
        i + 1
    return i

@app.route("/register/", methods=['POST', 'GET'])
def register():
    if request.method == 'GET':
        return render_template('register.html'), 200
    elif request.method == 'POST':
        if checkUser(request.form['userName']) == 0:
            s = "INSERT INTO users(userName, password) Values('" + request.form['userName'] + "', '" + bcrypt.hashpw(request.form['password'], bcrypt.gensalt())+"');" 
            db = get_db()
            db.cursor().execute(s)
            fileWrite = open('users.sql', 'a')
            fileWrite.write(s)
            fileWrite.close
            db.commit()
            return redirect(url_for('.home'))
        else:
            return render_template('register.html')

@app.route("/newTeam/", methods=['POST', 'GET'])
@requires_login
def newTeam():
    global uTeam
    global moneyRemaining
    players = []
    db = get_db()
    if request.args.get('position') is not None:
        sql = "SELECT playerID, playerName, value FROM player WHERE position='" + str(request.args.get('position')) + "' ORDER BY value DESC"
        for row in db.cursor().execute(sql):
            players.append("<tr><form name='selectedPlayers' action='' method='POST'><th><input type='text' name='playerName' value='" + str(row[1]) + "' readonly></input></th><td><input type='text' name='cost' value='" + str(row[2]) + "' readonly></input></td><td><button id='addPlayer' type='submit' class='btn-small'>+</button></form></tr>")
    player = request.form.get('playerName')
    if request.method == 'POST':
        if player not in uTeam and moneyRemaining >= float(request.form.get('cost')):
            if request.args.get('position') == 'Goalkeeper':
                if uTeam[1] == "" :
                    uTeam[1] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[2] == "" :
                    uTeam[2] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[3] == "" :
                    uTeam[3] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
            elif request.args.get('position') == 'Defender':
                if uTeam[4] == "" :
                    uTeam[4] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[5] == "" :
                    uTeam[5] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[6] == "" :
                    uTeam[6] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[7] == "" :
                    uTeam[7] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[8] == "" :
                    uTeam[8] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[9] == "" :
                    uTeam[9] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
            elif request.args.get('position') == 'Midfielder':
                if uTeam[10] == "" :
                    uTeam[10] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[11] == "" :
                    uTeam[11] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[12] == "" :
                    uTeam[12] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[13] == "" :
                    uTeam[13] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[14] == "" :
                    uTeam[14] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[15] == "" :
                    uTeam[15] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
            elif request.args.get('position') == 'Striker':
                if uTeam[16] == "" :
                    uTeam[16] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[17] == "":
                    uTeam[17] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[18] == "":
                    uTeam[18] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
                elif uTeam[19] == "":
                    uTeam[19] = player
                    moneyRemaining = moneyRemaining - float(request.form.get('cost'))
    return render_template('newTeam.html', players=players, team=uTeam, moneyRemaining=moneyRemaining), 200 

@app.route("/removeFromTeam/<name>/")
@requires_login
def remove(name=None):
    global uTeam
    global moneyRemaining
    num = uTeam.index(name)
    uTeam[num] = ""
    sql = "SELECT value FROM player WHERE playerName='" + name + "'"
    db = get_db()
    for row in db.cursor().execute(sql):
        moneyRemaining = moneyRemaining + row[0]
    if moneyRemaining > 250.0:
        moneyRemaining = 250.0
    return redirect(url_for('.newTeam'))

@app.route("/userHome/")
@requires_login
def userHome():
    user = session['who']
    db = get_db()
    global team
    if len(team) > 0:
        del team[:]
    pointsTotal=[]
    sql = "SELECT * FROM userTeam WHERE userName='" + user + "'"
    for row in db.cursor().execute(sql):
        team.append(row[3])
        team.append(row[4])
        team.append(row[5])
        team.append(row[6])
        team.append(row[7])
        team.append(row[8])
        team.append(row[9])
        team.append(row[10])
        team.append(row[11])
        team.append(row[12])
        team.append(row[13])
        team.append(row[14])
        team.append(row[15])
        team.append(row[16])
        team.append(row[17])
        team.append(row[18])
        team.append(row[19])
        team.append(row[20])
        team.append(row[21])
    for t in team:
        points = 0
        sql = "SELECT playerID FROM player WHERE playerName='" + t + "'"
        for row in db.cursor().execute(sql):
            sql2 = "SELECT * FROM playerMatchStatistics WHERE playerID='" + str(row[0]) + "'"
            for row in db.cursor().execute(sql2):
                points = points + 5*int(row[2])
                points = points + 3*int(row[3])
                points = points - 1*int(row[4])
                points = points - 2*int(row[5])
                if row[6] == "full":
                    points = points + 2
                elif row[6] == "SubOn" or row[6] == "SubOff":
                    points = points + 1
        pointsTotal.append(points)
    return render_template('userHome.html', user=user, team=team, pointsTotal=pointsTotal), 200

@app.route("/saveTeam/")
@requires_login
def saveTeam():
    global uTeam
    global moneyRemaining
    db = get_db()
    sql1 = "INSERT INTO userTeam(teamName,userName,gk1,gk2,gk3,df1,df2,df3,df4,df5,df6,md1,md2,md3,md4,md5,md6,st1,st2,st3,st4)"
    sql2 = "VALUES('" + request.args.get('teamName') + "','" + session['who'] + "','" + uTeam[1] + "','" + uTeam[2] + "','" + uTeam[3] + "','" + uTeam[4] + "','" + uTeam[5] + "','" + uTeam[6] + "','" + uTeam[7] + "','" + uTeam[8] + "','" + uTeam[9] + "','" + uTeam[10] + "','" + uTeam[11] + "','" + uTeam[12] + "','" + uTeam[13] + "','" + uTeam[14] + "','" + uTeam[15] + "','" + uTeam[16] + "','" + uTeam[17] + "','"  + uTeam[18] + "','" + uTeam[19] + "');"
    fullSQL = sql1 + sql2
    db.cursor().execute(fullSQL)
    db.commit()
    fileWrite = open('users.sql', 'a')
    fileWrite.write(fullSQL)
    fileWrite.close
    del uTeam[:]
    uTeam = []
    moneyRemaining = 250
    return redirect(url_for('.userHome'))

@app.route('/substitute/', methods=['GET','POST'])
@requires_login
def substitute():
    global team
    global uTeam
    global selectedTeam
    gk = []
    if len(gk) > 0 :
        del gk[:]
    df = []
    if len(df) > 0 :
        del df[:]
    md = []
    if len(md) > 0 :
        del md[:]
    st = []
    if len(st) > 0 :
        del st[:]
    if len(uTeam) > 0 :
        del uTeam[:]
    db = get_db()
    for t in team:
        sql = "SELECT playerName,position FROM player WHERE playerName='"
        sql = sql + t + "'"
        for row in db.cursor().execute(sql):
            if row[0] not in selectedTeam :
                if row[1] == "Goalkeeper" and len(gk) < 3:
                    gk.append("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + t + "'></div>")
                elif row[1] == "Defender" and len(df) < 6:
                    df.append("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + t + "'></div>")
                elif row[1] == "Midfielder" and len(md) < 6:
                    md.append("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + t + "'></div>")
                elif row[1] == "Striker" and len(st) < 4:
                    st.append("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + t + "'></div>")
    if request.method == 'POST':
        player = request.form.get('clickedPlayer')
        sql2 = "SELECT position FROM player WHERE playerName='" + player + "'"
        for row in db.cursor().execute(sql2):
            if row[0] == "Goalkeeper":
                gk.remove("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + player + "'></div>")
                if selectedTeam[0] == "" :
                    selectedTeam[0] = player
                elif selectedTeam[1] == "":
                    selectedTeam[1] = player
                elif selectedTeam[2] == "":
                    selectedTeam[2] = player
            elif row[0] == "Defender":
                df.remove("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + player + "'></div>")
                if selectedTeam[3] == "":
                    selectedTeam[3] = player
                elif selectedTeam[4] == "":
                    selectedTeam[4] = player
                elif selectedTeam[5] == "":
                    selectedTeam[5] = player
                elif selectedTeam[6] == "":
                    selectedTeam[6] = player
                elif selectedTeam[7] == "":
                    selectedTeam[7] = player
                elif selectedTeam[8] == "":
                    selectedTeam[8] = player
            elif row[0] == "Midfielder":
                md.remove("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + player + "'></div>")
                if selectedTeam[9] == "":
                    selectedTeam[9] = player
                elif selectedTeam[10] == "":
                    selectedTeam[10] = player
                elif selectedTeam[11] == "":
                    selectedTeam[11] = player
                elif selectedTeam[12] == "":
                    selectedTeam[12] = player
                elif selectedTeam[13] == "":
                    selectedTeam[13] = player
                elif selectedTeam[14] == "":
                    selectedTeam[14] = player
            elif row[0] == "Striker":
                st.remove("<div class='playerToSelect'><form action='' method='POST'><input type='submit' name='clickedPlayer' class='sButton' value='" + player + "'></div>")
                if selectedTeam[15] == "":
                    selectedTeam[15] = player
                elif selectedTeam[16] == "":
                    selectedTeam[16] = player
                elif selectedTeam[17] == "":
                    selectedTeam[17] = player
                elif selectedTeam[18] == "":
                    selectedTeam[18] = player
    return render_template('substitute.html', gk=gk, df=df, md=md, st=st, selectedTeam=selectedTeam), 200

@app.route("/saveSubs/")
def saveSubs():
    global selectedTeam
    db = get_db()
    sqlGK = "UPDATE userTeam SET gk1='" + selectedTeam[0] + "', gk2='" + selectedTeam[1] + "', gk3='" + selectedTeam[2] + "'"
    sqlDF = ",df1='" + selectedTeam[3] + "', df2='" + selectedTeam[4] + "', df3='" + selectedTeam[5] + "', df4='" + selectedTeam[6] + "', df5='" + selectedTeam[7] + "', df6='" + selectedTeam[8] + "'"
    sqlMD = ",md1='" + selectedTeam[9] + "', md2='" + selectedTeam[10] + "', md3='" + selectedTeam[11] + "', md4='" + selectedTeam[12] + "', md5='" + selectedTeam[13] + "', md6='" + selectedTeam[14] + "'"
    sqlST = ",st1='" + selectedTeam[15] + "', st2='" + selectedTeam[16] + "', st3='" + selectedTeam[17] + "', st4='" + selectedTeam[18] + "'"
    sqlUSR = "WHERE userName='" + session['who'] + "';"
    fullSQL = sqlGK+sqlDF+sqlMD+sqlST+sqlUSR
    db.cursor().execute(fullSQL)
    db.commit()
    fileWrite = open('users.sql', 'a')
    fileWrite.write(fullSQL)
    fileWrite.close()
    del selectedTeam[:]
    return redirect(url_for('.userHome'))


@app.route("/clubs/")
def clubs():
    clubs = []
    db = get_db()
    sql = "SELECT * from club"
    for row in db.cursor().execute(sql):
        clubs.append("<tr><th width='5%' style='background-color: rgb(" + str(row[3]) + ")'></th><th width='90%'><a href='" + str(row[0]) + "'>" + str(row[1]) + "</a></th></tr>") 
    return render_template('clubs.html', clubs=clubs), 200

@app.route("/clubs/<clubID>/")
def specificClub(clubID=None):
    club = []
    stadium = []
    gk = []
    df = []
    md = []
    st = []
    db = get_db()
    sql = "SELECT * FROM club WHERE clubID='" + clubID + "'"
    for row in db.cursor().execute(sql):
        club.append(str(row[0]))
        club.append(str(row[1]))
        club.append(str(row[2]))
        club.append(str(row[3]))
        club.append(str(row[4]))
        club.append(str(row[5]))
    sql2 = "SELECT * FROM stadium WHERE stadiumID=(SELECT stadium FROM club WHERE clubID='" + str(clubID) + "')"
    for row in db.cursor().execute(sql2):
        stadium.append(str(row[0]))
        stadium.append(str(row[1]))
        stadium.append(str(row[2]))
        stadium.append(str(row[3]))
    sql3 = "SELECT playerName, position FROM player WHERE club='" + clubID + "'ORDER BY value DESC"
    for row in db.cursor().execute(sql3):
        if row[1] == "Goalkeeper":
            gk.append("<tr><td>" + str(row[0]) + "</td></tr>")
        elif row[1] == "Defender":
            df.append("<tr><td>" + str(row[0]) + "</td></tr>")
        elif row[1] == "Midfielder":
            md.append("<tr><td>" + str(row[0]) + "</td></tr>")
        elif row[1] ==  "Striker":
            st.append("<tr><td>" + str(row[0]) + "</td></tr>")
    return render_template('specificClub.html', club=club, stadium=stadium, gk=gk, df=df, md=md, st=st), 200

@app.route("/specificStadium/<stadiumID>/")
def stadiumToTeam(stadiumID=None):
    db = get_db()
    sql = "SELECT clubID FROM club WHERE stadium='" + stadiumID + "'"
    for row in db.cursor().execute(sql):
        c = row[0]
    return redirect(url_for('.specificClub', clubID = c))

@app.route("/stadiums/")
def stadiums():
    stadiums = []
    db = get_db()
    sql = "SELECT * FROM stadium ORDER BY capacity DESC"
    for row in db.cursor().execute(sql):
        stadiums.append("<tr><th><a href='/specificStadium/" + str(row[0]) + "'>" + str(row[1]) + "</a></th><td>" + str(row[2]) + "</td><td>" + str(row[3]) + "</td></th>" )
    return render_template('stadiums.html', stadiums = stadiums), 200

@app.route("/gamePoints/", methods=['GET','POST'])
def gamePoints():
    db = get_db()
    gw = []
    cl = []
    hpl = []
    apl = []
    match = 0
    sql = "SELECT gameWeek, startDate FROM gameWeek"
    for row in db.cursor().execute(sql):
        gw.append("<option value='" + str(row[0]) + "'>" + str(row[1]) + "</option>")
    sql2 = "SELECT clubID, clubName FROM club"
    for row in db.cursor().execute(sql2):
        cl.append("<option value='" + str(row[0]) + "'>" + str(row[1]) + "</option>")
    gaW = request.args.get('gameWeek')
    hT = request.args.get('homeTeam')
    aT = request.args.get('awayTeam')
    sc = request.args.get('finalScore')
    if hT != '' and aT != '' and hT != aT and sc != '':
        sql3 = "Select playerID, playerName FROM player WHERE club='" + hT + "'"
        sql4 = "Select playerID, playerName FROM player WHERE club='" + aT + "'"
        for row in db.cursor().execute(sql3):
            hpl.append("<input type='text' width='20px' name='playerID' value='" + str(row[0]) + "'></input><input type='text' name='playerName' value='" +  str(row[1]) + "'></input><input type='text' name='goals' placeholder='goals'></input><input type='text' name='assists' placeholder='assists'></input><input type='text' name='yellowCards' placeholder='yellowCards'></input><input type='text' name='redCards' placeholder='redCards'></input><input type='text' name='played' placeholder='played'></input>")
        for row in db.cursor().execute(sql4):
            apl.append("<input type='text' width='20px' name='playerID' value='" + str(row[0]) + "'></input><input type='text' name='playerName' value='" +  str(row[1]) + "'></input><input type='text' name='goals' placeholder='goals'></input><input type='text' name='assists' placeholder='assists'></input><input type='text' name='yellowCards' placeholder='yellowCards'></input><input type='text' name='redCards' placeholder='redCards'></input><input type='text' name='played' placeholder='played'></input>")
        if request.method=='GET':
            s = "INSERT INTO match(season, gameWeek, homeTeam, awayTeam, finalScore) VALUES ('2016/17'," + gaW + "," + hT  + "," + aT + ",'" + str(sc) + "');"
            db.cursor().execute(s)
            for row in db.cursor().execute("SELECT matchID FROM match WHERE homeTeam=' + hT + "' AND awayTeam=' + aT + "'"):
                match = row[0]
            fileWrite = open('players.sql','a')
            fileWrite.write(s)
            fileWrite.close()
    if request.method == 'POST':
        sqlentry = "INSERT INTO playerMatchStatistics(playerID, matchID, goals, assists, yellowCards, redCards, played) VALUES(" + str(request.form.get('playerID')) + "," + str(match) + "," + str(request.form.get('goals')) + "," + str(request.form.get('assists')) + "," + str(request.form.get('yellowCards')) + "," + str(request.form.get('redCards')) + ",'" + str(request.form.get('played')) + "');"
        db.cursor().execute(sqlentry)
        fileWrite = open('players.sql', 'a')
        fileWrite.write(sqlentry)
        fileWrite.close()
    return render_template('gamePoints.html', gw=gw, cl=cl, hpl=hpl, apl=apl), 200

@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
