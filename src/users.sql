
INSERT INTO users(userName, password) Values('JR', '$2a$12$AU80yy60cafw6eKDcBT81ur4O9W6vLWeItA98AMArX2/RYDmQ1IJe');

INSERT INTO
userTeam(teamName,userName,gk1,gk2,gk3,df1,df2,df3,df4,df5,df6,md1,md2,md3,md4,md5,md6,st1,st2,st3,st4)VALUES('Ballotelli-tubbies','JR','David De Gea','Jordan Pickford','Eldin Jakupovic','Hector Bellerin','Virgil van Dijk','Marcos Alonso','Aaron Cresswell','Antonio Valencia','Wes Morgan','Sadio Mane','Juan Mata','Georginio Wijnaldum','Wilfried Zaha','Robert Snodgrass','Darren Fletcher','Sergio Agiero','Zlatan Ibrahimovic','Jermain Defoe','Victor Anichebe');
UPDATE userTeam SET gk1='David De Gea', gk2='Eldin Jakupovic', gk3='JordanPickford',df1='Virgil van Dijk', df2='Marcos Alonso', df3='Aaron Cresswell',df4='Antonio Valencia', df5='Hector Bellerin', df6='Wes Morgan',md1='SadioMane', md2='Robert Snodgrass', md3='Juan Mata', md4='Wilfried Zaha',md5='Darren Fletcher', md6='Georginio Wijnaldum',st1='Zlatan Ibrahimovic',st2='Sergio Agiero', st3='Victor Anichebe', st4='Jermain Defoe'WHERE userName='JR';
INSERT INTO users(userName, password) Values('josh', '$2a$12$AQRmzSM2yRxeDPy3/X6FsOUjp2K9TWdtAX3ZtqYyE6Brov7KZTgkO');