DROP TABLE if EXISTS userTeam;
DROP TABLE if EXISTS users;
DROP TABLE if EXISTS match;
DROP TABLE if EXISTS clubSeasonStatistics;
DROP TABLE if EXISTS playerMatchStatistics;
DROP TABLE if EXISTS seasonStatistics;
DROP TABLE if EXISTS gameWeek;
DROP TABLE if EXISTS club;
DROP TABLE if EXISTS season;
DROP TABLE if EXISTS player;
DROP TABLE if EXISTS stadium;

CREATE TABLE stadium (
    stadiumID INTEGER PRIMARY KEY AUTOINCREMENT,
    stadiumName text,
    capacity INTEGER,
    yearBuilt INTEGER
);

CREATE TABLE season (
    season text,
    PRIMARY KEY (season)
);

INSERT INTO season (season) VALUES ('2016/17');

CREATE TABLE club (
    clubID INTEGER PRIMARY KEY AUTOINCREMENT,
    clubName text,
    yearFormed INTEGER,
    homeColours text,
    stadium INTEGER,
    currentManager text,
    FOREIGN KEY (stadium) REFERENCES stadium(stadiumID)
);


CREATE TABLE player (
    playerID INTEGER PRIMARY KEY AUTOINCREMENT,
    playerName text,
    born text,
    position text,
    squadNumber text,
    countryFrom text,
    value real,
    club INTEGER,
    FOREIGN KEY (club) REFERENCES club(clubID)
);


CREATE TABLE gameWeek (
    season text not null,
    gameWeek INTEGER not null,
    startDate text,
    PRIMARY KEY (season, gameWeek),
    FOREIGN KEY (season) REFERENCES season(season)
);

INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 1, '13 Aug 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 2, '19 Aug 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 3, '27 Aug 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 4, '10 Sep 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 5, '16 Sep 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 6, '24 Sep 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 7, '30 Sep 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 8, '15 Oct 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 9, '22 Oct 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 10, '29 Oct 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 11, '05 Nov 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 12, '19 Nov 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 13, '26 Nov 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 14, '03 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 15, '10 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 16, '13 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 17, '17 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 18, '26 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 19, '30 Dec 2016');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 20, '02 Jan 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 21, '14 Jan 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 22, '21 Jan 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 23, '31 Jan 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 24, '04 Feb 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 25, '11 Feb 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 26, '25 Feb 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 27, '04 Mar 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 28, '11 Mar 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 29, '18 Mar 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 30, '01 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 31, '04 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 32, '08 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 33, '15 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 34, '22 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 35, '29 Apr 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 36, '06 May 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 37, '13 May 2017');
INSERT INTO gameWeek (season, gameWeek, startDate)
VALUES('2016/17', 38, '21 May 2017');

CREATE TABLE match (
    matchID INTEGER PRIMARY KEY AUTOINCREMENT,
    season text,
    gameWeek INTEGER,
    homeTeam INTEGER,
    awayTeam INTEGER,
    finalScore text,
    FOREIGN KEY (season) REFERENCES season(season),
    FOREIGN KEY (gameWeek) REFERENCES gameWeek(gameWeek),
    FOREIGN KEY (homeTeam) REFERENCES club(clubID),
    FOREIGN KEY (awayTeam) REFERENCES club(clubID)
);

CREATE TABLE playerMatchStatistics (
    playerID INTEGER not null,
    matchID INTEGER not null,
    goals INTEGER,
    assists INTEGER,
    yellowCards INTEGER,
    redCards INTEGER,
    played text,
    PRIMARY KEY (playerID, matchID),
    FOREIGN KEY (playerID) REFERENCES player(playerID),
    FOREIGN KEY (matchID) REFERENCES match(matchID)
);

CREATE TABLE seasonStatistics (
    season text not null,
    playerID INTEGER not null,
    scored INTEGER,
    assists INTEGER,
    savesMade INTEGER,
    yellowCards INTEGER,
    redCards INTEGER,
    starts INTEGER,
    subsituteAppearances INTEGER,
    PRIMARY KEY (season, playerID),
    FOREIGN KEY (season) REFERENCES season(season),
    FOREIGN KEY (playerID) REFERENCES player(playerID)
);

CREATE TABLE clubSeasonStatistics (
    season text,
    clubID INTEGER,
    win INTEGER,
    lose INTEGER,
    draw INTEGER,
    points INTEGER,
    goals INTEGER,
    assists INTEGER,
    yellowCards INTEGER,
    redCards INTEGER,
    PRIMARY KEY (season, clubID),
    FOREIGN KEY (season) REFERENCES season(season),
    FOREIGN KEY (clubID) REFERENCES club(clubID)
);

CREATE TABLE users (
    userName text,
    password text,
    PRIMARY KEY (userName)
);

CREATE Table userTeam (
    teamID INTEGER PRIMARY KEY AUTOINCREMENT,
    teamName text,
    userName text,
    gk1 text,
    gk2 text,
    gk3 text,
    df1 text,
    df2 text,
    df3 text,
    df4 text,
    df5 text,
    df6 text,
    md1 text,
    md2 text,
    md3 text,
    md4 text,
    md5 text,
    md6 text,
    st1 text,
    st2 text,
    st3 text,
    st4 text
);
