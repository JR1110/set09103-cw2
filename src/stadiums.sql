INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('The Stadium Of Light',49000, 1997);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Dean Court',11464, 1910);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('The Emirates Stadium',60432, 2006);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Turf Moor',22546, 1883);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Stamford Bridge',41631, 1877);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Selhurst Park',25456, 1924);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Goodison Park',39572, 1892);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('KCOM Stadium',25400, 2002);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('King Power Stadium',32312, 2002);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Anfield',54074, 1884);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Etihad Stadium',53000, 2002);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Old Trafford',75643, 1910);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('The Riverside Stadium',33746, 1995);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('St Marys Stadium',32505, 2001);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('bet365 Stadium',27902, 1997);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('The Liberty Stadium',21088, 2005);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('White Hart Lane',36284, 1899);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('Vicarage Road',21577, 1922);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('The Hawthorns',26850, 1900);
INSERT INTO stadium(stadiumName, capacity, yearBuilt)
VALUES('London Stadium',60000, 2012);


INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('AFC Bournemouth', 1899, '186,0,0',2, 'Eddie Howe');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Arsenal', 1886, '255,50,50',3, 'Arsene Wenger');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Burnley F.C.',1882,'128,22,56',4,'Sean Dyche');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Chelsea F.C.', 1905, '0,7,218',5,'Antonio Conte');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Crystal Palace', 1905, '0,7,196',6,'Alan Pardew'); 
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Everton F.C.', 1878, '0,134,236',7,'Ronald Koeman');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Hull City A.F.C.',1904,'230,192,0',8,'Mike Phelan');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Leicester City F.C.', 1884, '0,106,199',9,'Claudio Ranieri');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Liverpool F.C.',1892,'249,0,33',10,'Jurgen Klopp');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Manchester City F.C.', 1880,'0,196,226',11,'Pep Guardiola');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Manchester United',1878,'238,0,0',12,'Jose Mourinho');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Middlesbrough F.C.',1876,'255,0,0',13,'Aitor Karanka');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Southampton F.C.', 1885, '255,0,0',14,'Claude Puel');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Stoke City F.C.', 1863, '255,0,0', 15, 'Mark Hughes');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Sunderland A.F.C.', 1879, '245,0,0',1,'David Moyes');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Swansea City A.F.C', 1912, '255,255,255',16,'Bob Bradley');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Tottenham Hotspur F.C.', 1882, '255,255,255',17,'Mauricio Pochettino');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('Watford F.C.', 1881, '249,232,0',18,'Walter Mazzarri');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('West Bromwich Albion F.C.', 1878, '0,115,192', 19, 'Tony Pulis');
INSERT INTO club (clubName, yearFormed, homeColours, stadium, currentManager)
VALUES('West Ham United F.C.', 1895, '128,22,45', 20, 'Slaven Bilic');
